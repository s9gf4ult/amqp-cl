module Main where

import Control.Concurrent
import Control.Concurrent.MVar
import Control.Exception.Base
import Network.AMQP
import Data.Text ( Text )

rpc :: Channel
    -> Text
    -> Ack
    -> Message
    -> ((Message, Envelope) -> IO ())
    -> IO ()
rpc chan reciver ack msg' handler = do
    putStrLn "yoba"
    -- chan <- openChannel con
    (qname, _, _) <- declareQueue chan newQueue
                                      { queueDurable = False
                                      , queueExclusive = True }
    consumeMsgs chan qname ack handler
    -- v <- newEmptyMVar
    -- tag <- consumeMsgs chan qname ack
    --        (\a -> finally
    --               (handler a)
    --               (takeMVar v
    --                >>= cancelConsumer chan))
    -- putMVar v tag

    let msg = msg' { msgReplyTo = Just qname }
    publishMsg chan "" reciver msg
    putStrLn "message sent"

remqname :: Text
remqname = "echo"

main :: IO ()
main = do
    con <- openConnection "127.0.0.1" "/" "guest" "guest"
    chan <- openChannel con
    putStrLn "Connected!"
    let msg = newMsg { msgBody = "hello" }
    sequence_
        $ replicate 4000
        $ forkIO
        $ rpc chan remqname Ack msg $ \(recvmsg, env) -> do
            putStrLn "message received"
            print $ msgBody recvmsg
            ackEnv env

    threadDelay 5000000
    getLine
    closeConnection con
